import argparse
import asyncio
import hashlib
import logging
import os
import yaml
from aiohttp import TCPConnector, ClientSession, ClientTimeout

from app.compressor import to_zip
from app.scraper import Scraper, TaskQueueManager, Link


# import uvloop
# uvloop.install()


def init_logger():
    logging.basicConfig(
        filename='./scraper.log',
        filemode='w',
        level=logging.WARNING,
        format='%(levelname)s:%(message)s'
    )


def load_config():
    config_path = os.path.abspath(os.environ.get('CONFIG_PATH', './config.yml'))
    with open(config_path, 'r') as f:
        config = yaml.load(f.read(), Loader=yaml.FullLoader)
        return config


def make_dirs(path):
    if not os.path.isdir(path):
        os.makedirs(path)


def sha256(string):
    sha = hashlib.sha256()
    sha.update(string.encode())
    return sha.hexdigest()


def make_client_session(config):
    conn = TCPConnector(limit=config['connections_limit'], ssl=False)
    timeout = ClientTimeout(
        total=int(config.get('total_timeout_by_one_connect', 1800)),
        connect=int(config.get('connect_timeout', 10))
    )
    return ClientSession(connector=conn, timeout=timeout)


async def run(base_url, save_dir, config, max_depth=2):
    init_logger()
    cs = make_client_session(config)
    try:
        url_info = Link(base_url, 0, 1)
        queue = asyncio.Queue()
        scraper = Scraper(cs, queue, save_dir, config, max_depth=max_depth)
        queue.put_nowait(scraper.scraper(url_info))
        manager = TaskQueueManager(
            queue, max_tasks=config.get('connections_limit', 80)
        )
        await manager.run()
        # scraper.pbar.close()
        # print()
    finally:
        await cs.close()
        await asyncio.sleep(0.25)


def to_zip_(config, files_path, subdir):
    print('archiving:')
    make_dirs(config['path_to_save_archive'])
    save_path = os.path.join(config['path_to_save_archive'], f'{subdir}.zip')
    result = to_zip(files_path, save_path)

    print(result)
    return result


def run_scraper(url, depth, files_path=None, zip_=True):
    subdir = sha256(url)
    config = load_config()
    if not files_path:
        files_path = os.path.join(config['path_to_save_files'], subdir)
    make_dirs(files_path)

    try:
        asyncio.run(run(url, files_path, config, max_depth=depth))
    except KeyboardInterrupt:
        pass

    result = files_path

    if zip_:
        result = to_zip_(config, files_path, subdir)

    return result


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('url', help='url to parse', type=str)
    parser.add_argument('-z', '--zip', help="save as archive", action="store_true", default=True)
    parser.add_argument('-p', '--path', help='path to save files', type=str)
    parser.add_argument('-c', '--config', help='config path', type=str, default='./config.yml')
    parser.add_argument('-d', '--depth', help='nesting depth', type=int, default=2)

    args = parser.parse_args()
    config_path = os.path.abspath(args.config)
    if args.path:
        path = os.path.abspath(args.path)
    else:
        path = None

    run_scraper(args.url, args.depth, files_path=path, zip_=args.zip)
