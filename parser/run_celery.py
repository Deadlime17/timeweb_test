from scraper import to_zip_, make_dirs, load_config, run
from celery_app.database import Session, ScraperTasks
from celery.exceptions import SoftTimeLimitExceeded
from celery import Celery
import asyncio
import os


app = Celery('scraper_app',
             broker=os.environ.get('REDIS_URL', 'redis://localhost:6379/0'),
             backend=os.environ.get('REDIS_URL', 'redis://localhost:6379/0')
             )

app.conf.update(result_expires=3600)


def run_scraper(url, depth, task_id, files_path=None, zip=True):
    subdir = task_id
    config = load_config()
    if not files_path:
        files_path = os.path.join(config['path_to_save_files'], subdir)
    make_dirs(files_path)

    try:
        asyncio.run(run(url, files_path, config, max_depth=depth))
    except (SoftTimeLimitExceeded):
        pass

    to_zip_(config, files_path, subdir)
    return f'{subdir}.zip'


@app.task(name='run_scraper')
def run_scraper_task(task_id):
    session = Session()

    task = session.query(ScraperTasks).filter(ScraperTasks.task_id==task_id).first()
    if task:
        task.status = 'running'
        session.add(task)
        session.commit()
        try:
            result = run_scraper(task.url, task.depth, task.task_id)
        except:
            task.status = 'failed'
            session.add(task)
            session.commit()
            raise
        task.result = result
        task.status = 'done'
        session.add(task)
        session.commit()
        return result


if __name__ == '__main__':
    app.start()