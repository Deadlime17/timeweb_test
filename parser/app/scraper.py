from collections import namedtuple

import asyncio
import logging
from aiohttp import ClientSession

from .utils import *
from .writer import FileStreamWriter


class Link(namedtuple('Link', ['url', 'depth', 'attempt'])):
    __slots__ = ()

    def __str__(self):
        return self.url


class TaskQueueManager:
    def __init__(self, queue, max_tasks: int = float('inf')):
        self._queue = queue
        self._max_tasks = max_tasks
        self._tasks: List[asyncio.Task] = list()

    def _done(self):
        return self._queue.empty() and not self._tasks

    def _fetch_coroutine(self):
        if not self._queue.empty():
            return self._queue.get_nowait()

    async def _worker(self):
        while True:

            self._tasks = [task for task in self._tasks if not task.done()]
            if len(self._tasks) < self._max_tasks:
                if self._done():
                    break
                if coroutine := self._fetch_coroutine():
                    self._tasks.append(asyncio.create_task(coroutine))

            await asyncio.sleep(0.001)

    async def run(self):
        try:
            await self._worker()
        finally:
            for task in self._tasks:
                task.cancel()


class Scraper:
    def __init__(
            self, cs: ClientSession, queue,
            save_dir: str, config, max_depth: int = 2
    ):
        self.max_depth: int = max_depth
        self.save_dir: str = save_dir
        self.unique_urls: set[str] = set()
        self.counter = counter()
        self.counter.send((1, 0))
        self.queue: asyncio.Queue = queue
        self.cs = cs
        self.writer = FileStreamWriter
        self.allowed_content_types: List[str] \
            = config.get('allowed_content_types') or list()
        self.allow_redirects = config.get('allow_redirects', False)
        self.content_for_parsing = ('text/html', 'text/xhtml', 'text/xml')

    @staticmethod
    def _get_encoding(headers) -> str:
        encoding = 'utf-8'
        content_type = headers.get('Content-Type')
        if content_type and 'charset' in content_type:
            encoding = content_type.split('charset=')[-1]
        return encoding

    async def _process_page(self, page, link):
        for url in extract_urls(page, link.url):
            if domains_are_equal(url, link.url) \
                    and url not in self.unique_urls:
                self.unique_urls.add(url)
                new_link = Link(url, link.depth + 1, 0)
                await self.queue.put(self.scraper(new_link))
                self.counter.send((1, 0))

    async def _stream_save_file(self, receiver, path):
        async with self.writer(path) as destination:
            while chunk := await receiver.content.read(20000):
                await destination.write(chunk)

    async def _process_link(self, link, receiver):

        content_type = receiver.content_type
        content_is_allow = content_type in self.allowed_content_types
        if self.allowed_content_types and not content_is_allow:
            return

        path = url_as_path(link.url, content_type, self.save_dir)
        for_parsing = content_type in self.content_for_parsing

        if not for_parsing or link.depth == self.max_depth:
            await self._stream_save_file(receiver, path)

        else:
            data = await receiver.read()
            page = decode(data, self._get_encoding(receiver.headers))
            await self._process_page(page, link)
            async with self.writer(path) as destination:
                await destination.write(data)

    async def _get_link(self, link: Link):

        async with self.cs.get(
                link.url, allow_redirects=self.allow_redirects
        ) as receiver:

            if 200 >= receiver.status < 300:
                await self._process_link(link, receiver)

            else:
                logging.warning(f'status: {receiver.status} for {link}')

    async def scraper(self, link: Link):
        try:
            await self._get_link(link)
            self.counter.send((0, 1))

        except asyncio.exceptions.TimeoutError:
            if link.attempt < 3:
                new_url = Link(link.url, link.depth, link.attempt + 1)
                await self.queue.put(self.scraper(new_url))
                logging.warning(f'attempt #:{link.attempt} for {link}')

            else:
                self.counter.send((0, 1))
                logging.warning( f'attempts limit #:{link.attempt} for {link}')

        except Exception as e:
            self.counter.send((0, 1))
            raise
