from os import PathLike

import aiofiles
import os
from abc import ABC, abstractmethod


class AbstractStreamWriter(ABC):
    @abstractmethod
    def get_writer(self):
        pass

    @abstractmethod
    def write(self, data: bytes):
        pass

    @abstractmethod
    def close(self):
        pass


class FileStreamWriter(AbstractStreamWriter):
    def __init__(self, path):
        self.__file = None
        self.path: PathLike = path

    def _make_path(self):
        if not os.path.exists(os.path.split(self.path)[0]):
            os.makedirs(os.path.split(self.path)[0])

    async def get_writer(self):
        self._make_path()
        self.__file = await aiofiles.open(self.path, 'wb')
        return self

    async def write(self, data: bytes):
        await self.__file.write(data)

    async def close(self):
        await self.__file.close()

    async def __aenter__(self):
        return await self.get_writer()

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.close()

