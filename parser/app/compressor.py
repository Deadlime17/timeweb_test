from .utils import counter
import os
import zipfile
import shutil


def zipdir(path, ziph):
    count = counter()
    try:
        for _, _ , files in os.walk(path):
            count.send((len(files), 0))

        for root, dirs, files in os.walk(path):
            for file in files:
                ziph.write(
                    os.path.join(root, file),
                    os.path.relpath(
                        os.path.join(root, file),
                        os.path.join(path, '..')
                    )
                )
                os.remove(os.path.join(root, file))
                count.send((0, 1))
        shutil.rmtree(path)
    finally:
        count.close()


def to_zip(files_path, result_path):
    zipf = zipfile.ZipFile(result_path, 'w', zipfile.ZIP_DEFLATED)
    try:
        zipdir(files_path, zipf)
    finally:
        zipf.close()
    return result_path
