import os
import re
from tqdm.asyncio import tqdm
from typing import List, Generator, Tuple
from urllib.parse import urljoin, urlparse

LINK_REGEXP = re.compile(r'(?:href|src)=[\'"]?([^\'" >]+)')


def domains_are_equal(url: str, parent_url: str) -> bool:
    """
    www.google.com == google.com -> True
    """
    try:
        child = urlparse(url)
        parent = urlparse(parent_url)
        child_domain = child.netloc.split('.')[-2]
        parent_domain = parent.netloc.split('.')[-2]
        return child_domain == parent_domain
    except:
        return False


def normalize_url(url: str, parent_url: str) -> str:
    url.strip()
    if url.startswith('//'):
        url = 'http:' + url
    parsed_url = urlparse(url)
    if not parsed_url.netloc:
        url = urljoin(parent_url, url)
    return url


def extract_urls(content: str, parent_url: str) -> List[str]:
    urls = LINK_REGEXP.findall(content)

    for url in set(urls):
        yield normalize_url(url, parent_url)


def decode(content: bytes, encoding: str) -> str:
    if encoding is None:
        encoding = 'utf-8'
    content_ = content.decode(encoding)
    return content_


def url_as_path(url: str, content_type: str, base_dir) -> str:
    url = urlparse(url)

    # если url_path кончается именем файла с расширением,
    # то путь равен url
    if re.findall(r'(?=\w+\.\w{2,5}$).+', url.path):
        file_path = url.netloc + url.path

    # иначе имя файла = index с расширением из content_type
    else:
        path = url.netloc + url.path
        name = 'index.' + content_type.split("/")[-1]
        file_path = os.path.join(path, name)
    return os.path.join(base_dir, file_path)


def init_gen(func):
    def wrapper(*args, **kwargs):
        generator = func(*args, **kwargs)
        generator.send(None)
        return generator

    return wrapper


@init_gen
def counter() -> Generator[None, Tuple[int, int], None]:
    pbar = tqdm(total=0)

    try:
        while True:
            total, done = yield
            pbar.total += total
            pbar.update(done)
    finally:
        pbar.close()
