import os
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.automap import automap_base
from sqlalchemy import create_engine

Base = automap_base()
engine = create_engine(os.environ.get('DATABASE_URL', 'postgresql://postgres:password@localhost:5434/scraper'))
Base.prepare(engine, reflect=True)

ScraperTasks = Base.classes.scraper_tasks

Session = sessionmaker(bind=engine)

session = Session()

