import logging
import os


class Production:

    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_RECORD_QUERIES = True
    DEBUG = False

    SECRET_KEY = os.environ.get("SECRET_KEY", "12345")

    ARCHIVES_URL = os.environ.get('ARCHIVES_URL', 'http://0.0.0.0:8000/')

    @classmethod
    def init_app(cls, app):
        app.logger.setLevel(logging.INFO)


class Develop(Production):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', 'postgresql://postgres:password@localhost:5434/scraper')

    DEBUG = True
    TESTING = True

    @staticmethod
    def init_app(app):
        app.logger.setLevel(logging.DEBUG)


config_dict = {
    'prod': Production,
    'dev': Develop,
    'local': Develop
}
