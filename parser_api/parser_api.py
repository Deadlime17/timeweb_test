from flask_migrate import Migrate, upgrade
from app import create_app, db
import os
from config import config_dict

env_config = config_dict[os.environ.get('ENV', 'dev')]

app = create_app()
app.config.from_object(env_config)
migrate = Migrate(app, db)
from app.models.scraper import *


@app.cli.command()
def deploy():
    upgrade()


if __name__ == "__main__":
    app.run(
        port=int(os.environ.get('PORT', 6000)),
        host=os.environ.get('HOST','0.0.0.0')
    )
