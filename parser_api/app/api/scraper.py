from app.models.scraper import ScraperTasks, Status
from flask import request
from app import make_celery, db
from .scheme import scraper_task_schema, create_task_schema
from marshmallow import ValidationError
from app.api import scraper_blueprint
from uuid import uuid1

celery_app = make_celery()


def check_existence_the_task(task):
    task = ScraperTasks.query.filter(
        ScraperTasks.status.notin_([Status.done, Status.failed]),
        ScraperTasks.url == task.url, ScraperTasks.depth >= task.depth
    ).first()
    return task


def create_task(task):
    task.status = Status.created
    task.task_id = str(uuid1())
    db.session.add(task)
    db.session.commit()
    celery_task = celery_app.send_task( 'run_scraper', args=[task.task_id])
    return task


def get_task_by_id(task_id):
    return ScraperTasks.query.filter_by(task_id=task_id).first()


@scraper_blueprint.route('/task/', methods=['POST'], strict_slashes=False)
def route_create_task():
    data = request.get_json()
    try:
        task_schema = create_task_schema.load(data)
    except ValidationError as e:
        return e.normalized_messages(), 400
    task = ScraperTasks(**task_schema)
    if task_ := check_existence_the_task(task):
        return {'error': 'task already exists', 'task': scraper_task_schema.dump(task_)}, 409
    task = create_task(task)
    return scraper_task_schema.dump(create_task(task)), 201


@scraper_blueprint.route('/task/<task_id>', methods=['GET'])
def route_get_task_by_id(task_id):
    scraper_task_schema.load({'task_id': task_id})
    task = get_task_by_id(task_id)
    if task:
        # if task.status == Status.done:
        #     return request.host_url + task.result
        return scraper_task_schema.dump(task)
    return {"error": f"task with id '{task_id}' does not exists"}, 404
