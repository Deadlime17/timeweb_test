from marshmallow import fields, Schema, post_load, validate
from marshmallow.validate import URL, Range, Length
from marshmallow_sqlalchemy import SQLAlchemySchema
from flask import current_app


class CreateTaskSchema(SQLAlchemySchema):
    url = fields.Str(required=True, validate=URL(relative=False))
    depth = fields.Integer(required=False, default=2, validate=Range(0, 2))


class ScraperTaskSchema(SQLAlchemySchema):
    url = fields.Str(required=False, validate=URL(relative=False))
    task_id = fields.Str(required=False, validate=Length(max=64))
    status = fields.Str(required=False, validate=validate.OneOf(['created', 'running', 'done', 'failed']))
    result = fields.Function(lambda obj: obj.result if not obj.result else current_app.config['ARCHIVES_URL'] + str(obj.result))
    depth = fields.Integer(required=False, default=2, validate=Range(0, 2))

    class Meta:
        fields = ('task_id', 'url', 'depth', 'status', 'result')


scraper_task_schema = ScraperTaskSchema()
create_task_schema = CreateTaskSchema()