from app import db
import sqlalchemy as sa
from enum import Enum


class Status(Enum):
    created = 'created'
    running = 'running'
    done = 'done'
    failed = 'failed'

    def __str__(self):
        return self.value


class ScraperTasks(db.Model):
    __tablename__ = 'scraper_tasks'

    url = sa.Column(sa.String(2048), nullable=False)
    status = sa.Column(sa.Enum(Status), nullable=False)
    result = sa.Column(sa.String(200))
    depth = sa.Column(sa.Integer)
    task_id = sa.Column(sa.String(64), nullable=False, primary_key=True)
    timestemp = sa.Column(sa.TIMESTAMP, server_default=sa.func.now(), onupdate=sa.func.now())

    def __init__(self, **kwargs):
        super(ScraperTasks, self).__init__(**kwargs)
