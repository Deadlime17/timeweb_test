from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask import Flask
import os
from config import config_dict
from celery import Celery
from flask import Flask, jsonify
from werkzeug.exceptions import HTTPException
from werkzeug.exceptions import default_exceptions
import logging

db = SQLAlchemy()
migrate = Migrate()


def create_app(config_key='local'):
    app = Flask(__name__)

    app.config.from_object(config_dict[config_key])
    config_dict[config_key].init_app(app)

    db.init_app(app)
    migrate.init_app(app, db)

    from app.api import scraper_blueprint

    app.register_blueprint(scraper_blueprint, url_prefix='/scraper/')
    # for ex in default_exceptions:
    #     app.register_error_handler(ex, handle_error)
    @app.errorhandler(Exception)
    def handle_error(e):
        try:
            code = 500
            if isinstance(e, HTTPException):
                code = e.code
                return jsonify(error=e.description), code
            return jsonify(error='something went wrong'), code
        finally:
            app.logger.exception('error')
    return app


def make_celery():
    CELERY_BROKER_URL=os.environ.get('CELERY_BROKER_URL', 'redis://localhost:6380/0'),
    CELERY_RESULT_BACKEND=os.environ.get('CELERY_RESULT_BACKEND', 'redis://localhost:6380/0')

    celery = Celery('scraper_app',
                    broker=CELERY_BROKER_URL,
                    backend=CELERY_RESULT_BACKEND)
    return celery



