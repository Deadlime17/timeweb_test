Написать API на Flask для парсинга сайтов с двумя методами.
POST запрос получает адрес сайта и в ответ возвращает ID задачи.
GET запрос по ID задачи возвращает текущее состояние задачи. Когда задача выполнена, возвращает URL, по которому можно скачать архив.

В задание входит парсер, который пробегается по сайту с лимитированной вложенностью, например, до 3 уровней и сохраняет html/css/js и медиа файлы.

# Scraper


- Глубина скрапера задается от 0.
- При глубине 0 качает одну страницу(файл).
- При глубине 1 качает файл и если это текст, то находит все url в нем, ведущие на то же доменное имя что и родительский url и скачивает файлы по ним.
- По дефолту глубина равна 2. На таком уровне скачивается уже около 100000 файлов с https://en.wikipedia.org/wiki/Main_Page за 20 минут.
- Скачивание можно остановить Ctrl+C, начнется архивирование.



Для запуска скрапера из командной строки:
```sh
cd parser
pip install -r requirements.txt 
python scraper.py https://en.wikipedia.org/wiki/Main_Page -d 2
```


# Scraper API

```sh
docker-compose up
curl -X POST http://localhost:8000/scraper/task/ -d '{"url":"https://en.wikipedia.org/wiki/Main_Page", "depth": 2}' -H "Content-Type: application/json"  
curl -X GET http://localhost:8000/scraper/task/<task_id>
```
